%% LyX 2.3.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[italian]{article}
\usepackage[T1]{fontenc}
\usepackage{pifont}
\usepackage{units}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{babel}
\begin{document}
\title{Dall'equazione di Newton alle equazioni di Lagrange}
\author{Alessio Serraino}

\maketitle
Poniamoci in un sistema di riferimento inerziale\footnote{La definizione formale di \emph{sistema inerziale} \`{e} fondata sull'equazione
di Newton, tuttavia non \`{e} possibile dimostrare n\`{e} l'equazione
di Newton, n\`{e} l'esistenza di un sistema inerziale. Quindi assumeremo
come \emph{postulato} l'esistenza di un sistema inerziale, quindi
l'equazione di Newton, senza preoccuparci di quale sia questo riferimento.
A seconda dello studio da compiere si assumer\`{a} come inerziale
la Terra, piuttosto che il Sole, il centro della galassia...}, scrtta per un punto materiale di massa $m$, la celebre equazione
di Newton asserisce che:
\begin{equation}
\boldsymbol{F}^{\left(r\right)}=m\boldsymbol{a}\label{eq:newton}
\end{equation}
Dove $\boldsymbol{F}^{\left(r\right)}$ \`{e} la forza risultante
agente su $m$, ed $\boldsymbol{a}$ la sua accelerazione, intesa
come vettore.

\paragraph{Esistenza della reazione vincolare}

Supponiamo inoltre che il punto sia vincolato, ossia che esista una
relazione $f\left(x,y,z,t\right)=0$ fra le coordinate spaziali del
punto, ed eventualmente del tempo. Risulta chiaro che l'accelerazione
non pu\`{o} essere diretta in qualsiasi direzione dello spazio come
la (\ref{eq:newton}) potrebbe suggerire, ma deve essere sempre tale
da mantenere il punto sul vincolo. Per convincerci di ci\`{o} possiamo
scegliere, per semplicit\`{a}, il caso in cui $m$ sia un oggetto
appoggiato su un piano perfettamente orizzontale, e che sia soggetto
alla forza di gravit\`{a} $-mg\boldsymbol{k}$. Osserveremo che $m$
rimarrebbe immobile, ci\`{o} implica che la sua accelerazione sia
$\boldsymbol{0}$. Ma se abbiamo assunto la validit\`{a} della (\ref{eq:newton})
allora anche $\boldsymbol{F}^{\left(r\right)}$ deve essere $\boldsymbol{0}$.
Ci\`{o} implica che alla forza di gravit\`{a} si sommi un'altra forza
di pari direzione ed intensit\`{a}, ma verso opposto, che annulli
la risultante. Questa forza la chiameremo reazione vincolare, e la
indicheremo con $\boldsymbol{F}^{\left(\mathrm{v}\right)}$. In genere
dovremo assumere l'esistenza di $\boldsymbol{F}^{\left(\mathrm{v}\right)}$.
Chiameremo $\boldsymbol{F}$ forza attiva, una forza tale per cui
$\boldsymbol{F}^{\left(r\right)}=\boldsymbol{F}+\boldsymbol{F}^{\left(\mathrm{v}\right)}$
Quindi l'equazione di Newton che scriviamo \`{e}:
\begin{equation}
\boldsymbol{F}+\boldsymbol{F}^{\left(\mathrm{v}\right)}=m\boldsymbol{a}\label{eq:newtonbis}
\end{equation}


\paragraph{Il vincolo perfetto}

Supponiamo che il punto sia sempre soggetto ad un vincolo perfetto,
ovvero un vincolo dal quale \`{e} impossibile staccarsi, e che sia
perfettmente liscio, ossia il vincolo non esercita nessuna forza che
si contrapponga al libero scorrimento del punto su di esso. Ci\`{o}
si traduce nel fatto che $\boldsymbol{F}^{\left(\mathrm{v}\right)}$
\`{e} sempre normale al vincolo.

La condizione di normalit\`{a} della reazione vincolare si esprime
matematicamente con l'annullarsi del prodotto scalare fra la reazione
vincolare ed i vettori coordinati:
\[
\boldsymbol{F}^{\left(\mathrm{v}\right)}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}=0\qquad\forall i\in\left\{ 1,\ldots,n\right\} 
\]

\`{E} anche interessante esaminare il caso di trasformazione di coordinate
(esporremo nel caso di singola particella, l'estensione a sistemi
di $n$ particelle \`{e} analoga). Si avr\`{a} una funzione di immersione
$\boldsymbol{x}\left(q_{1},q_{2},q_{3}\right)$ che prende un vettore
di $3$ componenti, le coordinate nel nuovo sistema, e restituisce
le $3$ componenti cartesiane. La condizione di vincolo perfetto si
pu\`{o} applicare anche in questo caso: la condizione di ortogonalit\`{a}
di $\boldsymbol{F}^{\left(\mathrm{v}\right)}$ rispetto a ciascuno
dei vettori coordinati ci spinge a cercare un vettore di $\mathbb{R}^{3}$
che sia ortogonale ad una base di vettori indipendenti, e nello spazio
tridimensionale esso \`{e} solo il vettore nullo. Il metodo lagrangiano
tratta senza distinzione anche il caso di particella libera soggetta
ad un cambio di coordinate.

\paragraph{Il metodo lagrangiano}

Il metodo langrangiano consiste nel proiettare l'equazione del moto
lungo i vettori coordinati, in modo da eliminare l'incognita $\boldsymbol{F}^{\left(\mathrm{v}\right)}$.
Proiettare significa moltiplicare scalarmente, quindi per ciascuno
dei vettori coordinati moltiplichiamo scalarmente, ossia scriviamo
$n$ equazioni.
\begin{equation}
\boldsymbol{F}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}+\underset{=0}{\underbrace{\cancel{\boldsymbol{F}^{\left(\mathrm{v}\right)}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}}}}=m\boldsymbol{a}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}\qquad\forall i\in\left\{ 1,\ldots,n\right\} \label{eq:lagrange1}
\end{equation}


\paragraph{Il numero di equazioni}

Questo paragrafo potrebbe non essere richiesto nella risposta alla
domanda, tuttavia \`{e} un argomento strettamente connesso con il
discorso, ed \`{e} in genere chiesto durante l'orale.

L'equazione di Newton \`{e} un'equazione differenziale del secondo
ordine, vettoriale, che, per la singola particella, contiene sempre
$3$ incognite: nel caso di particella libera sono le $3$ coordinate
libere della particella, perch\`{e} la reazione vincolare $\boldsymbol{F}^{\left(\mathrm{v}\right)}\equiv\boldsymbol{0}$
vale identicamente $\boldsymbol{0}$.

Nel caso di particella vincolata alla superficie si descrive il suo
moto (almeno in una carta locale) con sole due coordinate libere,
le funzioni $q_{1}\left(t\right)$ e $q_{2}\left(t\right)$. Tuttavia
si introduce come incognita il modulo della reazione vincolare, sappiamo
gi\`{a} che \`{e} diretto normalmente alla superficie, l'unica informazione
che ci manca sono il verso ed il modulo, queste informazioni costituiscono
il modulo ed il segno di una ulteriore incognita scalare.

Nel caso di particella vincolata ad una linea si riduce di uno il
numero di funzioni del tempo che descrivono il moto nell'unica coordinata
libera, $q_{1}\left(t\right)$, tuttavia servono due ulteriori funzioni
per descrivere la reazione vincolare $\boldsymbol{F}^{\left(\mathrm{v}\right)}$.
Di essa sappiamo solo che giace nel piano normale alla curva, e per
descrivere un vettore nel piano sono necessarie due coordinate.

Quindi il numero di incongite \`{e}, per ogni tipo di vincolo, sempre
uguale a $3$. \`{E} semplice convincersi che nel caso di sistema
di $n$ particelle le incognite sono $3n$. Le equazioni che dovremo
scrivere per risolvere le equazioni del moto sar\`{a} sempre un sistema
di $3n$ equazioni (differenziali del secondo ordine).

Il procedimento lagrangiano in effetti ci permette di ottenere sistematicamente
le $3n$ equazioni richieste: un'equazione per ciascuna delle $q$,
le coordinate libere, da queste si potr\`{a} ricavare direttamente
il movimento nelle coordinate $q$, infine tutte le restanti equazioni
si ricavano imponendo che $\boldsymbol{F}^{\left(\mathrm{v}\right)}\cdot\frac{\partial\boldsymbol{x}}{\partial q}=0$.
Queste ultime equazioni forniscono la soluzione alla restante parte
delle incognite, ossia quelle della reazione vincolare. Come abbiamo
visto la somma del numero di coordinate libere (gradi di libert\`{a})
e del numero di incognite che servono per descrivere $\boldsymbol{F}^{\left(\mathrm{v}\right)}$
\`{e} costante e costantemente pari a $3n$.

\paragraph{Il binomio Lagrangiano}

Dell'equazione (\ref{eq:lagrange1}) proviamo a sviluppare il secondo
membro, e scriviamo l'accelerazione come $\boldsymbol{a}=\frac{\mathrm{d}\boldsymbol{v}}{\mathrm{d}t}$.
Usando la formula di integrazione per parti $\frac{\mathrm{d}f}{\mathrm{d}t}\cdot g=\frac{\mathrm{d}}{\mathrm{d}t}\left(fg\right)-f\cdot\frac{\mathrm{d}g}{\mathrm{d}t}$
(valida anche per funzioni vettoriali), scriviamo che:
\begin{equation}
\frac{\mathrm{d}\boldsymbol{v}}{\mathrm{d}t}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}=\frac{\mathrm{d}}{\mathrm{d}t}\left(\boldsymbol{v}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}\right)-\boldsymbol{v}\cdot\frac{\mathrm{d}}{\mathrm{d}t}\left(\frac{\partial\boldsymbol{x}}{\partial q_{i}}\right)\label{eq:temp1}
\end{equation}
Dobbiamo sviluppare ulteriormente questa formula, tuttavia ci sono
due ugualianze non banali, che dimostriamo prima di procedere.

\subparagraph{L'espressione della velocit\`{a}}

Conviene innanzitutto osservare la seguente formula per esprimere
la velocit\`{a}:
\begin{equation}
\boldsymbol{v}=\frac{\mathrm{d}\boldsymbol{x}\left(q_{1},\ldots,q_{n},t\right)}{\mathrm{d}t}=\sum\frac{\partial\boldsymbol{x}}{\partial q_{i}}\dot{q_{i}}+\frac{\partial\boldsymbol{x}}{\partial t}\label{eq:velocita}
\end{equation}
Si \`{e} ottenuta tramite la regola di derivazione della funzione
composta: $\boldsymbol{v}\triangleq\frac{\mathrm{d}\boldsymbol{x}}{\mathrm{d}t}$,
ed $\boldsymbol{x}$ \`{e} a sua volta dipendente (in genere) da ciascuna
delle $q$ secondo la formula di immersione. Per generalit\`{a} consideriamo
anche la dipendenza dal tempo.

Nel caso tempo-invariante (e \emph{solo} per il caso tempo-invariante,
$\frac{\partial\boldsymbol{x}}{\partial t}=0$), la (\ref{eq:velocita})
ci dice che $\boldsymbol{v}$ \`{e} una combinazione lineare dei vettori
coordinati $\frac{\partial\boldsymbol{x}}{\partial q_{i}}$, secondo
i coefficienti $\dot{q_{i}}$. Poich\`{e} i vettori coordinati sono
sempre tangenti al vincolo anche la velocit\`{a} sar\`{a} tangente
al vincolo, come ci si pu\`{o} aspettare per un vincolo fisso.

Anche nel caso generale di vincolo tempo-dipendente possiamo osservare
che $\frac{\partial\boldsymbol{x}}{\partial q_{i}}$ non \`{e} altro
che il coefficiente del termine $\dot{q_{i}}$ nella (\ref{eq:velocita}),
quindi la derivata di $\boldsymbol{v}$ su $\dot{q_{i}}$ che compare
nella formula (\ref{eq:temp1}) la possiamo sviluppare come: $\frac{\partial\boldsymbol{v}}{\partial\dot{q_{i}}}=\frac{\partial\boldsymbol{x}}{\partial q_{i}}$.

\subparagraph{Lemma 1}

Prima di procedere conviene dimostrare il seguente lemma. Applichiamo
la regola di derivazione della funzione composta al termine $\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial\boldsymbol{x}}{\partial q_{i}}$,
che \`{e} funzione di $q_{i}$:
\begin{align*}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial\boldsymbol{x}}{\partial q_{i}} & \overset{\text{\text{\ding{192}}}}{=}\sum_{j=1}^{n}\frac{\partial}{\partial q_{j}}\frac{\partial\boldsymbol{x}}{\partial q_{i}}\dot{q_{j}}+\frac{\partial}{\partial t}\frac{\partial\boldsymbol{x}}{\partial q_{i}}\overset{\text{\text{\ding{193}}}}{=}\sum_{j=1}^{n}\frac{\partial}{\partial q_{i}}\frac{\partial\boldsymbol{x}}{\partial q_{j}}\dot{q_{j}}+\frac{\partial}{\partial q_{i}}\frac{\partial\boldsymbol{x}}{\partial t}=\\
 & \overset{\text{\text{\ding{194}}}}{=}\frac{\partial}{\partial q_{i}}\left(\sum_{j=1}^{n}\frac{\partial\boldsymbol{x}}{\partial q_{i}}\dot{q_{i}}+\frac{\partial\boldsymbol{x}}{\partial t}\right)\overset{\text{\text{\ding{195}}}}{=}\frac{\partial\boldsymbol{v}}{\partial q_{i}}
\end{align*}
Dove al passaggio \ding{192} si \`{e} applicata la regola di derivazione
della funzione composta alla funzione $\frac{\partial\boldsymbol{x}}{\partial q_{i}}$,
che \`{e} in genere dipendente da tutte le $q$, e dal tempo, quindi
con $q_{j}$ scorriamo tutte le $q$, deriviamo la funzione $\frac{\partial\boldsymbol{x}}{\partial q_{i}}$
su $q_{j}$, e moltiplichiamo per $\dot{q_{j}}$, come asserisce la
formula, infine lo stesso viene fatto anche per la dipendenza dal
tempo. Si presti attenzione che si intende derivata su $q_{j}$ solo
del termine $\frac{\partial\boldsymbol{x}}{\partial q_{i}}$, non
del prodotto $\frac{\partial\boldsymbol{x}}{\partial q_{i}}\dot{q_{j}}$.
Al passaggio \ding{193} si \`{e} applicato il teorema di Schwarz quindi
\`{e} stato invertito l'ordine di derivazione. Dobbiamo implicitamente
supporre una sufficiente regolarit\`{a} di $\boldsymbol{x}\left(q_{1},\ldots,q_{n},t\right)$,
funzione di immersione del vincolo. Al passaggio \ding{194} si \`{e}
usata la propriet\`{a} di linearit\`{a} della derivata (derivata della
somma \`{e} somma delle derivate). Infine al passaggio \ding{195}
si \`{e} semplicemente sostituita la (\ref{eq:velocita}).

\subparagraph{Il binomio Lagrangiano}

quindi abbiamo che $\frac{\partial\boldsymbol{x}}{\partial q_{i}}=\frac{\partial\boldsymbol{v}}{\partial\dot{q_{i}}}$,
sostituendo questa informazione nella (\ref{eq:temp1}) si ha:
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\left(\boldsymbol{v}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}\right)-\boldsymbol{v}\cdot\frac{\mathrm{d}}{\mathrm{d}t}\left(\frac{\partial\boldsymbol{x}}{\partial q_{i}}\right)=\frac{\mathrm{d}}{\mathrm{d}t}\left(\boldsymbol{v}\cdot\frac{\partial\boldsymbol{v}}{\partial\dot{q_{i}}}\right)-\boldsymbol{v}\cdot\frac{\partial\boldsymbol{v}}{\partial q_{i}}\label{eq:temp2}
\end{equation}
A questo punto trasformiamo le espressioni del tipo $\boldsymbol{v}\cdot\partial\boldsymbol{v}=\partial\left(\nicefrac{1}{2}\boldsymbol{v}\cdot\boldsymbol{v}\right)=\partial\left(\nicefrac{1}{2}v^{2}\right)$,
formula che si dimostra banalmente con la regola di derivazione del
prodotto. Moltiplicando per la massa, come vuole il secondo membro
della (\ref{eq:lagrange1}) ci si riconduce ad una espressione dipendente
dalla sola energia cinetica. 
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial}{\partial\dot{q_{i}}}\left(\frac{m}{2}\boldsymbol{v}\cdot\boldsymbol{v}\right)-\frac{\partial}{\partial q_{i}}\left(\frac{m}{2}\boldsymbol{v}\cdot\boldsymbol{v}\right)=\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial\dot{q_{i}}}-\frac{\partial T}{\partial q_{i}}\label{eq:binomio-lagrangiano}
\end{equation}
Quest'ultimo risultato \`{e} noto come binomio lagrangiano.

\subparagraph{Le equazioni di Lagrange}

Utilizzando il risultato del binomio sviluppiamo l'espressione (\ref{eq:lagrange1})
ottenuta precedentemente:
\begin{equation}
\boldsymbol{F}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}=\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial\dot{q_{i}}}-\frac{\partial T}{\partial q_{i}}\label{eq:lagrange1-1}
\end{equation}
Per una forza attiva $\boldsymbol{F}$ derivante da potenziale vale
$\boldsymbol{F}=-\nabla V$, ossia $\boldsymbol{F}=-\frac{\partial V}{\partial\boldsymbol{x}}$.
Con un abuso di notazione si semplificano i $\partial\boldsymbol{x}$
del primo termine, anche se quello che si applica in realt\`{a} \`{e}
il teorema della derivazione della funzione composta, ma al contrario
rispetto al verso usuale. Bisogna vedere $V$ come funzione delle
$q$ attraverso la formula di immersione:
\[
\boldsymbol{F}\cdot\frac{\partial\boldsymbol{x}}{\partial q_{i}}=-\frac{\partial V}{\partial q_{i}}
\]
Quindi la (\ref{eq:lagrange1-1}) diventa, portando tutto a destra:
\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial\dot{q_{i}}}-\frac{\partial T}{\partial q_{i}}+\frac{\partial V}{\partial q_{i}}=0\label{eq:lagrange1-1-1}
\end{equation}
A questo punto dobbiamo applicare la distributivit\`{a} della derivata,
e scrivere che $-\frac{\partial T}{\partial q_{i}}+\frac{\partial V}{\partial q_{i}}=-\frac{\partial T-V}{\partial q_{i}}$,
il che ci suggerisce di introdurre la funzione langrangiana $L$ definendola
come:
\begin{equation}
L=T-V\label{eq:definizione-lagrangiana}
\end{equation}

Infine non ci resta che osservare che $\frac{\partial T}{\partial\dot{q_{i}}}=\frac{\partial L}{\partial\dot{q_{i}}}$,
in quanto $V$ non dipende dalle $\dot{q}$. Abbiamo quindi ottenuto
il seguente risultato, noto come equazione di Lagrange:

\[
\text{\fbox{\ensuremath{\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial L}{\partial\dot{q}}-\frac{\partial L}{\partial q}=0}}}
\]

Abbiamo cos\`{\i} dimostrato l'equivalenza fra le soluzioni dell'equazione
di Newton e l'equazione di Lagrange.
\end{document}
